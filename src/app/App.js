import React, { Component } from 'react';
import Task from './components/Task';
import Form from './components/Form'

class App extends Component {

  render() {
    return (
      <div>
        {/* NAVIGATION */}
        <nav className="light-blue darken-4">
          <div className="container">
            <div className="nav-wrapper">
              <a href="#" className="brand-logo">MERN Stack</a>
            </div>
          </div>
        </nav>

        <div className="container">
          
          <div className="row">
            <div className="col s5">
              {/* FORM */}
              <Form />
            </div>
            <div className="col s7">
               {/* TASKS */}
                <Task />
            </div>
          </div>

          
        </div>
      </div>
    )
  }
}

export default App;