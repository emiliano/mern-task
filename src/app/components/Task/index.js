import React, { Component } from 'react'

//import Form from './Form';
import Table from './List'

class Task extends Component {

  constructor() {
    super();
    this.state = {
      title: '',
      description: '',
      _id: '',
      tasks: []
    }
  }

  componentDidMount() {
    this.fetchTasks();
  }

  fetchTasks() {
    fetch('/api/tasks')
      .then(res => res.json())
      .then(data => {
        this.setState({tasks: data});
        console.log(this.state.tasks);
      });
  }

  render(){
    return <Table tasks={this.state.tasks} />
  }

}

export default Task