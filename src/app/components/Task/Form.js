import React, { Component } from 'react'

class Form extends Component {
    constructor(){
        super();
        this.state = {
            task: {
                
            }
        }

        this.handleChange = this.handleChange.bind(this);
        this.addTask = this.addTask.bind(this);
    }

    handleChange(e) {
        const { name, value } = e.target;
        this.setState({
            [name]: value
        });
    }

    addTask(e) {
        e.preventDefault();
        if(this.state._id) {
            fetch(`/api/tasks/${this.state._id}`, {
                method: 'PUT',
                body: JSON.stringify({
                    title: this.state.title,
                    description: this.state.description
                }),
                headers: {
                    'Accept': 'application/json',
                    'Content-Type': 'application/json'
                }
            })
            .then(res => res.json())
            .then(data => {
                window.M.toast({html: 'Task Updated'});
                this.setState({_id: '', title: '', description: ''});
                this.fetchTasks();
            });
        } else {
            fetch('/api/tasks', {
                method: 'POST',
                body: JSON.stringify(this.state),
                headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json'
                }
            })
            .then(res => res.json())
            .then(data => {
              console.log(data);
              window.M.toast({html: 'Task Saved'});
              this.setState({title: '', description: ''});
              this.fetchTasks();
            })
            .catch(err => console.error(err));
        }
    
      }

  render() {
      {/*this.state.title*/} 
      {/*this.state.description*/}
    return (
        <div className="card">
            <div className="card-content">
                <form onSubmit={this.addTask}>
                    <div className="row">
                        <div className="input-field col s12">
                            <input name="title" 
                                onChange={this.handleChange} 
                                value=""
                                type="text" placeholder="Task Title" autoFocus/>
                        </div>
                    </div>
                    <div className="row">
                        <div className="input-field col s12">
                            <textarea 
                                name="description" 
                                onChange={this.handleChange} 
                                value="" cols="30" rows="10" 
                                placeholder="Task Description" 
                                className="materialize-textarea"></textarea>
                        </div>
                    </div>

                    <button type="submit" className="btn light-blue darken-4">
                    Send 
                    </button>
                </form>
            </div>
        </div>
    )
  }
}


export default Form