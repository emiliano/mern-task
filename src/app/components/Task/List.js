import React, { Component } from 'react'

class List extends Component {

    deleteTask(id) {
        if(confirm('Are you sure you want to delete it?')) {
            fetch(`/api/tasks/${id}`, {
                method: 'DELETE',
                headers: {
                    'Accept': 'application/json',
                    'Content-Type': 'application/json'
                }
            })
            .then(res => res.json())
            .then(data => {
                console.log(data);
                M.toast({html: 'Task deleted'});
                this.fetchTasks();
            });
        }
    }

    editTask(id) {
        fetch(`/api/tasks/${id}`)
          .then(res => res.json())
          .then(data => {
            console.log(data);
            this.setState({
              title: data.title,
              description: data.description,
              _id: data._id
            });
          });
    }

  render() {
    return (
      <div>
        <table>
            <thead>
                <tr>
                <th>Title</th>
                <th>Description</th>
                </tr>
            </thead>
            <tbody>
            { 
                this.props.tasks.map(task => {
                    return (
                        <tr key={task._id}>
                            <td>{task.title}</td>
                            <td>{task.description}</td>
                            <td>
                                <button 
                                    onClick={() => this.deleteTask(task._id)} 
                                    className="btn light-blue darken-4">
                                    <i className="material-icons">delete</i> 
                                </button>
                                <button 
                                    onClick={() => this.editTask(task._id)} 
                                    className="btn light-blue darken-4" style={{margin: '4px'}}>
                                    <i className="material-icons">edit</i>
                                </button>
                            </td>
                        </tr>
                    )
                })
            }
            </tbody>
        </table>
      </div>
    )
  }
}

export default List