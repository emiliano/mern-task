import React, { Component } from 'react'
import Input from './Input'
import TextArea from './TextArea'
import Button from './Button'

class Form extends Component {
    constructor() {
        super()

        this.state = {
            task: {
                title: '',
                description: '',
                _id: ''
            }
        }
        this.handleInput = this.handleInput.bind(this)
        this.handleFormSubmit = this.handleFormSubmit.bind(this)
        this.handleClearForm = this.handleClearForm.bind(this)
    }

    handleInput(e) {
        let { value, name } = e.target
        this.setState(prevState => {
            return{
                task: {
                    ...prevState.task, [name]:value
                }
            } 
        }, () => console.log(this.state.task))
    }

    handleFormSubmit(e){
        e.preventDefault()
        let {task} = this.state

        console.info('Save: ', task)
    }

    handleClearForm(e){
        e.preventDefault()
        this.setState({
            task: {
                title: '',
                description: '',
                _id: ''
            }
        })
    }

    render() {
        //console.info('TAREA: ', this.state.task)
        return (
            <form className='container-fluid' onSubmit={this.handleFormSubmit}>
                <Input
                    inputtype={"text"}
                    title={"Task Name"}
                    name={"title"}
                    value={this.state.task.title}
                    placeholder={"Task name"}
                    handleChange={this.handleInput}
                />
                <TextArea
                    title={"Description"}
                    rows={10}
                    value={this.state.task.description}
                    name={"description"}
                    handleChange={this.handleInput}
                    placeholder={"Describe your task"}
                />
                <Button
                    action={this.handleFormSubmit}
                    type={"primary"}
                    title={"Submit"}
                    style={buttonStyle}
                />{" "}
                {/*Submit */}
                <Button
                    action={this.handleClearForm}
                    type={"secondary"}
                    title={"Clear"}
                    style={buttonStyle}
                />
            </form>
        )
    }
}

const buttonStyle = {
    margin: "10px 10px 10px 10px"
};

export default Form
